import React, { Component } from 'react';
import { Link } from "react-router-dom";
import kitaplar from './data/kitaplar';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';


class Create extends React.Component {

    render() {      
    return (
        <div className="container">
            <br/>
            <form>
                <div className="form-group">
                    <label className="font-weight-bold">KİTAP ADI</label>
                    <input class="form-control" type="text" id="input" placeholder="Kitap Adı"/>
                </div>
                <div className="form-group">
                    <label className="font-weight-bold">YAZAR ADI</label>
                    <input class="form-control" type="text" id="input" placeholder="Yazar Adı"/>
                </div>
                <div className="form-group">
                    <label className="font-weight-bold">YAYIN EVİ ADI</label>
                    <input class="form-control" type="text" id="input" placeholder="Yayın Evi Adı" />
                </div>
                <div className="form-group">
                <label className="font-weight-bold">KONU</label>
                <textarea class="form-control" id="input" rows="2"></textarea>
                </div>
                <div className="form-group">
                    <label className="font-weight-bold">YAYIN YILI</label>
                    <input class="form-control" type="number" min="1800" placeholder="1800" id="input"/>
                </div>
            </form>

            <Link to="/"><button className="btn btn-success">Ekle</button></Link>
            <Link to="/"><button className="btn btn-danger">Geri Dön</button></Link>
            
            
        </div>
    );
    }
    
  }
  
  export default Create;