import React, { Component } from 'react';
import { Link } from "react-router-dom";
import kitaplar from './data/kitaplar';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

class Books extends Component {
  
  render() {
    var data = kitaplar;
    return (

      <div className="container">

        <div>
          <table className="table table-boarded">
            <thead className="thead-dark">
              <tr>
                <th>#</th>
                <th>KİTAP ADI</th>
                <th>İNCELE</th>
              </tr>
            </thead>
            <tbody>
              {data.map(function (item, index) {
                return <tr>
                  <td>{item.ID}</td>
                  <td>{item.KitapAdi}</td>
                  <td>
                    <Link to={'/detail/' + item.ID}><button className="btn btn-primary">Detay</button></Link>

                  </td>
                </tr>

              })}
            </tbody>
          </table>
          <Link to="/create"><button className="btn btn-success">Kitap Ekle</button></Link>
          
        </div>
      </div>
    );
  }
}

export default Books;