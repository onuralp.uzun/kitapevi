import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import kitaplar from './data/kitaplar';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import yazarlar from './data/yazarlar';
import yayinevleri from './data/yayinevleri';

const Details = ({ match }) => {

  var kitap ;
  kitaplar.forEach(element => {
    if (element.ID == match.params.id) {
      console.log(element);
      kitap = element;
      }
  });
  var yazar;
  yazarlar.forEach(element => {
    if(element.YazarID == kitap.YazarID){
      yazar = element;
    }
  });
  var yayinevi;
  yayinevleri.forEach(element => {
    if(element.YayinEviID == kitap.YayinEviID){
      yayinevi = element;
    }
  });

  return (
    <div className="title">
    <table className="table table-boarded">
    <thead className="thead-light">
              <tr>
                <th>Kitap Adı</th>
                <th>Yazarı</th>
                <th>Yayın Evi</th>
                <th>Konu</th>
                <th>Yayın Yılı</th>
              </tr>
    </thead>
    <tbody>
               <tr>
               <td>{kitap.KitapAdi}</td>
               <td>{yazar.YazarAdi}</td>
               <td>{yayinevi.YayinEviAdi}</td>
               <td>{kitap.Konu}</td>
               <td>{kitap.YayimYili}</td>
               </tr>
      </tbody>
      </table>
      <Link to="/"><button className="btn btn-danger">Geri Dön</button></Link>
    </div>
  )
}


export default Details;