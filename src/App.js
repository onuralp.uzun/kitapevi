import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import logo from './images/logo.png'
import Books from './Book';
import Detail from './Detail';
import Create from './Create';


class App extends Component {
  render() {
    
    return (
      
        <div className="container">
          <div className="row">
          <img src={logo} alt="logo" className="logo col-md-5"/>
          </div>
          <hr/>
          <Router>
          <div>
          <Route path="/" exact component={Books} />
          <Route path="/detail/:id" component={Detail} />
          <Route path="/create" component={Create}/>
          </div>
          </Router>
          <hr />



        </div>
      
    );
  }
}

export default App;