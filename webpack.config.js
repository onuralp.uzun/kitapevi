module.export = {
    entry :'./src/index.js',
}
module : {
    loaders : [
        {
            test: /\.css$/,
            loader: 'style-loader!css-loader',
            test : /\.js$/,
            loader : 'babel-loader',
            exclude : /node_modules/
        }
    ]
}